# OnCom Market Wordpress Deployment Project

## Overview
This project outlines the setup and deployment of a WordPress application using GitLab CI/CD, Docker, and Zabbix for monitoring. The deployment process targets two environments: staging and production, utilizing Docker Compose for container orchestration. Additionally, the project includes setting up alerting with Zabbix and Telegram.

## Table of Contents
- [OnCom Market Wordpress Deployment Project](#oncom-market-wordpress-deployment-project)
  - [Overview](#overview)
  - [Table of Contents](#table-of-contents)
  - [Prerequisites](#prerequisites)
  - [Setup Instructions](#setup-instructions)
  - [Pipeline Stages](#pipeline-stages)
  - [Pipeline Configuration](#pipeline-configuration)
  - [GitLab CI/CD Variables](#gitlab-cicd-variables)

## Prerequisites
- Two servers with access (`server1` and `server2`).
- Docker and Docker Compose on your PC.
- Access to GitLab and permissions to create repositories and pipelines.

## Setup Instructions
1. **Install Docker Engine on `server1` and `server2`:**
    Create docker contexts with access to servers.

2. **Register GitLab Runner on `server1`:**
    Run the following command to register the runner:
    ```sh
    docker --context=server1 run -d -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner:alpine register \
        --non-interactive \
        --url https://gitlab.com/ \
        --registration-token $REGISTRATION_TOKEN \
        --executor docker \
        --description "OnCom Market Wordpress runner" \
        --tag-list "docker" \
        --docker-image alpine:latest \
        --docker-privileged \
        --docker-volumes "/certs/client"
    ```

3. **Deploy Zabbix Server and Agent:**
    Clone [zabbix repository](https://github.com/zabbix/zabbix-docker) from GitHub.
    After configuring variables, use Docker Compose to deploy the Zabbix server and agent on `server1` and `server2` respectively:
    ```sh
    docker --context=server1 compose -f docker-compose_v3_ubuntu_pgsql_latest.yaml up -d
    docker --context=server2 compose -f docker-compose_v3_ubuntu_pgsql_latest.yaml up zabbix-agent -d
    ```

4. **Configure Telegram Alerting in Zabbix:**
    Follow the Zabbix documentation to set up Telegram notifications.

## Pipeline Stages

- **build**: Builds Docker images with Wordpress.
- **pre-deploy**: Deploys the proxy(required upon first start).
- **deploy**: Deploys the application.

## Pipeline Configuration

The GitLab CI/CD pipeline (`.gitlab-ci.yml`) is configured to deploy to both staging and production environments. The deployment stages are as follows:

- Staging Deployment: Automatically triggered for new commits.
- Production Deployment: Manually triggered through GitLab's UI.

To manually trigger the stage pipeline from any branch, you need to set the variable `RUN_STAGING=true` during the pipeline run.
The pipeline uses several GitLab CI/CD variables to manage configurations and secrets.

For the first run, you need to execute the `pre-deploy` stage to start the proxy, which will redirect requests from different domain names to the `stage` and `prod` environments respectively. To start the proxy, you need to set the `RUN_PROXY=true` variable. If the run is from the `main` or `dev` branch, nothing else needs to be specified. Otherwise, you need to set the `RUN_STAGING` variable.

## GitLab CI/CD Variables

The following environment variables are used in the pipeline:
   - `CI_REGISTRY`
   - `CI_REGISTRY_PASSWORD`
   - `CI_REGISTRY_USER`
   - `ENV_FILE_PROD`
   - `ENV_FILE_STAGE`
   - `PROXY_ENV_FILE`
   - `SERVER_HOST`
   - `SERVER_SSH_PORT`
   - `SERVER_USER`
   - `SSH_PRIVATE_KEY`

The `ENV_FILE_PROD` and `ENV_FILE_STAGE` contain the following configuration:
```yml
DB_ROOT_PASSWORD=""
DB_USER=""
DB_PASSWORD=""
DB_PORT=""
```

The `PROXY_ENV_FILE` contain the following configuration:
```yml
SERVER_DOMAIN_NAME_STAGE="example.com"
SERVER_DOMAIN_NAME_PROD="example.com"
```
