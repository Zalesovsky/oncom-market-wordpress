FROM wordpress:6.5.4-php8.2
WORKDIR /var/www/html
COPY ./wp-content/ ./wp-content/
RUN chmod -R 755 ./wp-content/